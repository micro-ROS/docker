#! /bin/sh 

# defaults
registry="registry.gitlab.com"
namespace="micro-ros/docker"
version="latest"


if [ "${git_image_id}" != "" ]
then
  GIT_IMAGE_ARG="-t ${git_image_id}"
fi

# overrides from CI server, if present
if [ "$CI_REGISTRY" != "" ]
then
  registry="${CI_REGISTRY}"
fi

if [ "${CI_PROJECT_PATH}" != "" ]
then
  namespace=$(echo $CI_PROJECT_PATH|tr '[:upper:]' '[:lower:]')
fi

if [ "x${BASE_IMAGE_VERSION}" != "x" ]
then
  version="$BASE_IMAGE_VERSION"
else
  if [ "${CI_COMMIT_REF_SLUG}" != "" ]
  then
    version=${CI_COMMIT_REF_SLUG}
  fi
fi


if [ "${http_proxy}" != "" ]
then
  if echo $http_proxy | grep -q -E '(127.0.0.1|localhost):'
  then
    PROXY_ARG="--build-arg http_proxy=$(echo ${http_proxy} | sed 's/127.0.0.1:/172.17.0.1:/'|sed 's/localhost:/172.17.0.1:/') \
      --build-arg https_proxy=$(echo ${https_proxy} | sed 's/127.0.0.1:/172.17.0.1:/' | sed 's/localhost:/172.17.0.1:/')"
  else
    PROXY_ARG="--build-arg http_proxy=${http_proxy} \
     --build-arg https_proxy=${https_proxy}"
  fi
fi

# GO
cmd="docker build --build-arg registry=$registry --build-arg namespace=$namespace \
 --build-arg version=$version \
 $PROXY_ARG $GIT_IMAGE_ARG $@ ."
echo $cmd
exec $cmd
